﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLINQ.Models.ApiModels
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
