﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLINQ.Models.ApiModels
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}
