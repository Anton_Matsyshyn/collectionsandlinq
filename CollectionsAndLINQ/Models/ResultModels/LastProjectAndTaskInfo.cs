﻿using CollectionsAndLINQ.Models.ApiModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLINQ.Models.ResultModels
{
    public class LastProjectAndTaskInfo
    {
        public User Author { get; set; }
        public Project LastProject { get; set; }
        public int TaskCount { get; set; }
        public int UnfinishedOrCanceledTaskCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
