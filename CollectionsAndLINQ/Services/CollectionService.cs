﻿using CollectionsAndLINQ.Models.ApiModels;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ThreadTask = System.Threading.Tasks.Task;
using ApplicationTask = CollectionsAndLINQ.Models.ApiModels.Task;
using System.Collections.Immutable;
using System.Runtime.InteropServices.WindowsRuntime;
using CollectionsAndLINQ.Models.ResultModels;

namespace CollectionsAndLINQ.Services
{
    public class CollectionService
    {
        private readonly HttpService _httpService;

        private IEnumerable<Project> _projects;
        private IEnumerable<ApplicationTask> _tasks;
        private IEnumerable<Team> _teams;
        private IEnumerable<User> _users;
        public CollectionService()
        {
            _httpService = new HttpService();
        }

        public CollectionService(IEnumerable<Project> projects,
                                 IEnumerable<ApplicationTask> tasks,
                                 IEnumerable<Team> teams,
                                 IEnumerable<User> users)
        {
            _httpService = new HttpService();

            _projects = projects;
            _tasks = tasks;
            _teams = teams;
            _users = users;
        }
        
        /// <summary>
        /// This is solution for 1st task
        /// </summary>
        public Dictionary<int, int> GetProjectsIdAndTaskCount(int projectOwnerId)
        {
            var result = _projects.Where(p => p.AuthorId == projectOwnerId)
                                 .GroupJoin(_tasks,
                                            p => p.Id,
                                            t => t.ProjectId,
                                            (p, tList) => new
                                            {
                                                ProjectId = p.Id,
                                                TaskCount = tList.Count()
                                            })
                                 .ToDictionary(res => res.ProjectId, res => res.TaskCount);

            return result;
        }

        /// <summary>
        /// This is solution for 2nd task
        /// </summary>
        public IEnumerable<ApplicationTask> GetUsersTask(int userId)
        {
            var result = _tasks.Where(t => t.PerformerId == userId &&
                                     t.Name.Count() < 45);
            return result;
        }

        /// <summary>
        /// This is solution for 3d task
        /// </summary>
        public IEnumerable<(int Id, string Name)> GetFinishedUserTasks(int userId)
        {
            var filteredTasks = _tasks.Where(t => t.PerformerId == userId &&
                                            t.FinishedAt.Year == 2020 &&
                                            t.State == TaskState.Finished)
                                     .Select(t => (t.Id, t.Name));

            return filteredTasks;
        }

        /// <summary>
        /// This is solution for 4th task
        /// </summary>
        public IEnumerable<(int TeamId, string TeamName, IOrderedEnumerable<User> Users)> GetTeamsWithUsers()
        {
            var result = _teams.GroupJoin(_users,
                                         t => t.Id,
                                         u => u.TeamId.GetValueOrDefault(),
                                         (t, uList) => 
                                         (
                                             t.Id,
                                             t.Name,
                                             uList.Where(u => 2020 - u.Birthday.Year > 10)
                                                  .OrderByDescending(u => u.RegisteredAt)
                                         ))
                                .Where(o => o.Item3.Any(u => 2020 - u.Birthday.Year > 10));

            return result;
        }

        /// <summary>
        /// This is solution for 5th task
        /// </summary>
        public IEnumerable<User> GetUsersWithTasks()
        {
            var result = _users.OrderBy(u => u.FirstName)
                               .GroupJoin(_tasks,
                                     u => u.Id,
                                     t => t.PerformerId,
                                     (u, tList) =>
                                     {
                                         u.Tasks = tList.OrderByDescending(t => t.Name.Length);
                                         return u;
                                     }
                                     );

            return result;
        }

        /// <summary>
        /// This is solution for 6th task
        /// </summary>
        public LastProjectAndTaskInfo GetLastUserProject(int userId)
        {
            var result = _users.GroupJoin(_projects,
                                          u => u.Id,
                                          p => p.AuthorId,
                                          (u, pList) =>
                                          {
                                              u.Projects = pList;
                                              return u;
                                          })
                               .GroupJoin(_tasks,
                                          u => u.Id,
                                          t => t.PerformerId,
                                          (u, tList) =>
                                          {
                                              u.Tasks = tList;
                                              return u;
                                          })
                                .Where(u => u.Id == userId)
                                .Select(u => new LastProjectAndTaskInfo
                                    {
                                        Author = u,
                                        LastProject = u.Projects?.GroupJoin(_tasks,
                                                                        p => p.Id,
                                                                        t => t.ProjectId,
                                                                        (p, tList) =>
                                                                        {
                                                                            p.Tasks = tList;
                                                                            return p;
                                                                        })
                                                            .OrderBy(p => p.CreatedAt).LastOrDefault(),
                                        TaskCount = u.Projects.OrderBy(p => p.CreatedAt).LastOrDefault()?.Tasks.Count() ?? 0,
                                        UnfinishedOrCanceledTaskCount = u.Tasks.Where(t => t.State != TaskState.Finished).Count(),
                                        LongestTask = u.Tasks?.OrderBy(t => t.FinishedAt - t.CreatedAt).LastOrDefault()
                                     })
                                .FirstOrDefault();

            return result;
        }

        /// <summary>
        /// This is solution for 7th task
        /// </summary>
        public IEnumerable<ProjectAndTeamInfo> GetProjectWithTeam()
        {
            var result = _projects.GroupJoin(_tasks,
                                        p => p.Id,
                                        t => t.ProjectId,
                                        (p, tList) =>
                                        {
                                            p.Tasks = tList;
                                            return p;
                                        })
                                   .Join(_teams,
                                        p => p.TeamId,
                                        t => t.Id,
                                        (p, t) =>
                                        {
                                            p.Team = t;
                                            return p;
                                        })
                                   .GroupJoin(_users,
                                              p => p.TeamId,
                                              u => u.TeamId,
                                              (p, uList) =>
                                              {
                                                  p.Team.Users = uList;
                                                  return p;
                                              })
                                   .Select(p => new ProjectAndTeamInfo
                                   {
                                       Project = p,
                                       LongestTask = p.Tasks.OrderBy(t => t.Description.Length).LastOrDefault(),
                                       ShortestTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                                       TeammatesCount = p.Description.Length > 20 || p.Tasks.Count() < 3 ? p.Team.Users.Count() : default
                                   });

            return result != null ? result : new ProjectAndTeamInfo[0];
        }
        public async ThreadTask InitializeCollections()
        {
            _projects = await _httpService.GetAllEntitiesAsync<Project>();
            _tasks = await _httpService.GetAllEntitiesAsync<ApplicationTask>();
            _teams = await _httpService.GetAllEntitiesAsync<Team>();
            _users = await _httpService.GetAllEntitiesAsync<User>();
        }
    }
}
