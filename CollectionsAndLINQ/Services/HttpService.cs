﻿using CollectionsAndLINQ.Models.ApiModels;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using CollectionsAndLINQ.Settings;
using Newtonsoft.Json;

using ThreadTask = System.Threading.Tasks.Task;
using ApplicationTask = CollectionsAndLINQ.Models.ApiModels.Task;

namespace CollectionsAndLINQ.Services
{
    public class HttpService
    {
        private readonly HttpClient _httpClient;
        public HttpService()
        {
            _httpClient = new HttpClient() { BaseAddress = new Uri(NetSettings.ApiEndpoint) };
        }

        public async Task<IEnumerable<T>> GetAllEntitiesAsync<T>()
        {
            string endpoint = "";
            endpoint = GetEndpointFromType(typeof(T));

            var response = await _httpClient.GetAsync(endpoint);
            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var entities = JsonConvert.DeserializeObject<IEnumerable<T>>(await content.ReadAsStringAsync());
                return entities;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        public async Task<T> GetEntityAsync<T>(int id)
        {
            string endpoint = "";
            endpoint = GetEndpointFromType(typeof(T));

            var response = await _httpClient.GetAsync(endpoint+id);
            var content = response.Content;

            await CheckResponse(response);

            try
            {
                var entity = JsonConvert.DeserializeObject<T>(await content.ReadAsStringAsync());
                return entity;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong:");
                Console.WriteLine(ex.Message);
            }

            return default(T);
        }

        private async ThreadTask CheckResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return;

            var message = await response.Content.ReadAsStringAsync();

            throw new Exception(response.StatusCode + ":\t" + message);
        }

        private string GetEndpointFromType(Type type)
        {
            if(type == typeof(Project))
            {
                return NetSettings.ProjectsEndpoint;
            }

            if(type == typeof(ApplicationTask))
            {
                return NetSettings.TasksEndpoint;
            }

            if(type == typeof(Team))
            {
                return NetSettings.TeamsEndpoint;
            }

            if(type == typeof(User))
            {
                return NetSettings.UsersEndpoint;
            }

            return "";
        }
    }
}
