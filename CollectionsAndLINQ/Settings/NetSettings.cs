﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsAndLINQ.Settings
{
    public static class NetSettings
    {
        public static string ApiEndpoint { get; set; } = "https://bsa20.azurewebsites.net/api/";
        public static string ProjectsEndpoint { get; set; } = "Projects/";
        public static string TasksEndpoint { get; set; } = "Tasks/";
        public static string TeamsEndpoint { get; set; } = "Teams/";
        public static string UsersEndpoint { get; set; } = "Users/";
    }
}
